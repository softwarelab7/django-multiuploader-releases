SLAB7_VERSION = 3
VERSION = (1, 2, 1, 'SoftwareLab7', SLAB7_VERSION)
__version__ = '.'.join(map(str, VERSION))
