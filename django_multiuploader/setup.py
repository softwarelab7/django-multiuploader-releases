import os
from setuptools import setup

from multiuploader import __version__

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-multiuploader',
    version=__version__,
    packages=['multiuploader'],
    include_package_data=True,
    license='None',  # example license
    description='A simple Django app to conduct Web-based polls.',
    long_description='',
    url='https://bitbucket.org/softwarelab7/django-multiuploader',
    author='SoftwareLab7',
    author_email='contact@softwarelab7.com',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: None', # example license
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    install_requires=[
        'South>=0.8.1',
    ]
)
